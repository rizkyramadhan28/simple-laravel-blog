<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class ClientController extends Controller
{
    public function index() {

        $posts = Post::latest()->get();

        return view("client.index", compact("posts"));
    }
}
