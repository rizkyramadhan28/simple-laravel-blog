<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->paginate(5);
        $title = "Posts";

        return view("admin.post.index", compact("posts", "title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        $title = "Create New Post";

        return view("admin.post.create", compact("categories", "tags", "title"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "title" => "required|max:255",
            "category_id" => "required",
            "content" => "required",
            "image" => "required"
        ]);

        $image = $request->image;
        $imageNewName = uniqid() . "_" . $image->getClientOriginalName();

        $post = Post::create([
            "title" => $request->title,
            "category_id" => $request->category_id,
            "content" => $request->content,
            "image" => "uploads/posts/" . $imageNewName,
            "slug" => Str::slug($request->title),
            "user_id" => Auth::id()
        ]);

        $post->tags()->attach($request->tags);

        $image->move(public_path() .  "/uploads/posts/", $imageNewName);

        return redirect(route("post.index"))->with("success", $post->title . " has been created successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        $title = "Edit Post";

        return view("admin.post.edit", compact("post", "categories", "tags", "title"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            "title" => "required|max:255",
            "category_id" => "required",
            "content" => "required"
        ]);

        $updatedPostData = [
            "title" => $request->title,
            "category_id" => $request->category_id,
            "content" => $request->content,
            "slug" => Str::slug($request->title)
        ];

        if ($request->hasFile("image")) {
            $image = $request->image;
            $imageNewName = uniqid() . "_" . $image->getClientOriginalName();
            $image->move("uploads/posts/", $imageNewName);

            $updatedPostData["image"] = "uploads/posts/" . $imageNewName;
        }

        $post->tags()->sync($request->tags);
        $post->update($updatedPostData);

        return redirect(route("post.index"))->with("success", $post->title . " has been updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect(route("post.index"))->with("success", $post->title . " has been moved to trashed posts successfully.");
    }

    public function showTrashedPosts() {
        $posts = Post::onlyTrashed()->paginate(5);
        $title = "Trashed Posts";

        return view("admin.post.trashed_posts", compact("posts", "title"));
    }

    public function restoreTrashedPosts($id) {
        $restoredPost = Post::withTrashed()->where("id", $id)->first();
        $restoredPost->restore();

        return redirect(route("post.index"))->with("success", $restoredPost->title . " has been restored successfully.");
    }

    public function deleteTrashedPost($id) {
        $restoredPost = Post::withTrashed()->where("id", $id)->first();

        $imageName = substr($restoredPost->image, 14);
        $filePath = public_path() . "/uploads/posts/" . $imageName;

        if(File::exists($filePath)) {
            File::delete($filePath);
        }

        $restoredPost->forceDelete();

        return redirect(route("post.index"))->with("success", $restoredPost->title . " has been deleted successfully.");
    }
}
