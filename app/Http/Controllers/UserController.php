<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(5);
        $title = "Users";

        return view("admin.user.index", compact("users", "title"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Create New User";

        return view("admin.user.create", compact("title"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|max:35",
            "email" => "required|email|unique:users,email",
            "password" => "required|min:6|max:255",
        ]);

        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            "role" => $request->role
        ]);

        return redirect(route("user.index"))->with("success", $user->name . " has been created successfully!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $title = "Edit User";

        return view("admin.user.edit", compact("user", "title"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            "name" => "required|max:35",
            "email" => "required|email|unique:users,email," . $user->id,
            "role" => "required"
        ]);

        $userNewData = [
            "name" => $request->name,
            "email" => $request->email,
            "role" => $request->role,
        ];

        if ($request->input("password")) {
            $userNewData["password"] = Hash::make($request->password);
        } else {
            $userNewData["password"] = $user->password;
        }

        $user->update($userNewData);

        return redirect(route("user.index"))->with("success", $user->name . " has been updated successfully!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect(route("user.index"))->with("success", $user->name . " has been deleted successfully!");
    }
}
