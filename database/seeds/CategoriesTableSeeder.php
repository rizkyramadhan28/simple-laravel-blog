<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use Illuminate\Support\Str;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();

        Category::create(["name" => "Laravel", "slug" => Str::slug("laravel")]);
        Category::create(["name" => "Javascript", "slug" => Str::slug("javascript")]);
        Category::create(["name" => "Vue JS", "slug" => Str::slug("vue js")]);
    }
}
