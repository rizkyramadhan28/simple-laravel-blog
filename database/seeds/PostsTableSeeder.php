<?php

use Illuminate\Database\Seeder;
use App\Models\Post;
use Illuminate\Support\Str;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::truncate();

        Post::create([
            "category_id" => 1,
            "title" => "This is First Post",
            "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit fuga et corrupti voluptatum quis voluptate recusandae numquam a, officia iure tempora modi, error rem aut fugiat, pariatur eum ratione? Laudantium.",
            "image" => "https://codelatte.org/wp-content/uploads/2019/11/laravel-thumbnail-800x445.jpg",
            "slug" => Str::slug("This is First Post"),
            "user_id" => 1
        ]);

        Post::create([
            "category_id" => 2,
            "title" => "This is Second Post",
            "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit fuga et corrupti voluptatum quis voluptate recusandae numquam a, officia iure tempora modi, error rem aut fugiat, pariatur eum ratione? Laudantium.",
            "image" => "https://codelatte.org/wp-content/uploads/2019/11/laravel-thumbnail-800x445.jpg",
            "slug" => Str::slug("This is Second Post"),
            "user_id" => 1
        ]);

        Post::create([
            "category_id" => 3,
            "title" => "This is Third Post",
            "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit fuga et corrupti voluptatum quis voluptate recusandae numquam a, officia iure tempora modi, error rem aut fugiat, pariatur eum ratione? Laudantium.",
            "image" => "https://codelatte.org/wp-content/uploads/2019/11/laravel-thumbnail-800x445.jpg",
            "slug" => Str::slug("This is Third Post"),
            "user_id" => 1
        ]);
    }
}
