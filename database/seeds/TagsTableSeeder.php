<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;
use Illuminate\Support\Str;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::truncate();

        Tag::create(["name" => "Laravel", "slug" => Str::slug("laravel")]);
        Tag::create(["name" => "Javascript", "slug" => Str::slug("javascript")]);
        Tag::create(["name" => "Vue JS", "slug" => Str::slug("vue js")]);
    }
}
