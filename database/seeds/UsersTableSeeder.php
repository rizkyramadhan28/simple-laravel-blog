<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            "name" => "Super Admin",
            "email" => "admin@admin.com",
            "password" => bcrypt("12345678")
        ]);
    }
}
