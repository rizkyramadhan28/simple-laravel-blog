@extends('admin.index')

@section('title', $title)

@section('content')

<div class="col-12">
    <div class="card">
        @include('admin.partials.alerts')

        <div class="card-header">
            <h4>{{ $title }}</h4>

            <div class="card-header-action">
                <a href="{{ route('category.create') }}" class="btn btn-success">Create</a>
            </div>
        </div>

        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </tr>

                    @foreach ($categories as $category)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->created_at }}</td>
                        <td>{{ $category->updated_at }}</td>
                        <td>
                            <form action="{{ route('category.destroy', $category) }}" method="POST">
                                @csrf
                                @method("DELETE")

                                <a href="{{ route('category.edit', $category) }}"
                                    class="btn btn-sm btn-primary">Edit</a>

                                <button type="submit" class="btn btn-sm btn-warning">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <div class="card-footer text-right">
            <nav class="d-inline-block">
                {{ $categories->links()}}
            </nav>
        </div>
    </div>
</div>

@endsection