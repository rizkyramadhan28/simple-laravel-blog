@extends('admin.index')

@section('title', $title)

@section('content')
<div class="row">
    <div class="col-12">
        @if ($errors->any())
        <div class="alert alert-danger">
            <p><b>Error</b></p>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="card">
            <div class="card-header">
                <h4>{{ $title }}</h4>
            </div>

            <div class="card-body">
                <form id="editPostForm" action="{{ route('post.update', $post) }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    @method("PUT")

                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
                        <div class="col-sm-12 col-md-7">
                            <input value="{{ $post->title }}" name="title" type="text" class="form-control"
                                placeholder="Please enter post title">
                        </div>
                    </div>

                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Category</label>
                        <div class="col-sm-12 col-md-7">
                            <select name="category_id" class="form-control selectric">
                                @foreach ($categories as $category)
                                <option value="{{ $category->id }}" @if ($post->category_id == $category->id)
                                    selected
                                    @endif
                                    >{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tags</label>
                        <div class="col-sm-12 col-md-7">
                            <select name="tags[]" class="form-control select2" multiple="">
                                @foreach ($tags as $tag)
                                <option value="{{ $tag->id }}" @foreach ($post->tags as $selectedTag)
                                    @if ($selectedTag->id == $tag->id)
                                    selected
                                    @endif
                                    @endforeach
                                    >{{ $tag->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                        <div class="col-sm-12 col-md-7">
                            <textarea name="content" class="form-control"
                                placeholder="Please enter post content">{{ $post->content }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Thumbnail</label>
                        <div class="col-sm-12 col-md-7">
                            <img src="{{ asset($post->image) }}" alt="{{ $post->title }}" class="img-thumbnail"
                                style="height: 200px;">
                        </div>
                    </div>


                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                        <div class="col-sm-12 col-md-7">
                            <input value="{{ $post->image }}" type="file" type="image" name="image" id="image">
                        </div>
                    </div>

                    <div class="form-group row mb-4">
                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                        <div class="col-sm-12 col-md-7">
                            <button onclick="submitPost()" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                    <script>
                        function submitPost() {
                            document.getElementById("editPostForm").submit();
                        }
                    </script>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection