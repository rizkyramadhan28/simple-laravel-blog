@extends('admin.index')

@section('title', $title)

@section('content')

<div class="col-12">
    <div class="card">
        @include('admin.partials.alerts')

        <div class="card-header">
            <h4>{{ $title }}</h4>

            <div class="card-header-action">
                <a href="{{ route('post.create') }}" class="btn btn-success">Create</a>
            </div>
        </div>

        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tr>
                        <th>No.</th>
                        <th>Author</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Tags</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>

                    @foreach ($posts as $post)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $post->user->name }}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->category->name }}</td>
                        <td>
                            @foreach ($post->tags as $tag)
                            <div class="badge badge-pill badge-primary mb-1">{{ $tag->name }}</div>
                            @endforeach
                        </td>
                        <td>
                            <img src="{{ asset($post->image) }}" alt="{{ $post->title }}" class="img-thumbnail"
                                style="height: 100px;">
                        </td>
                        <td>
                            <form action="{{ route('post.destroy', $post) }}" method="POST">
                                @csrf
                                @method("DELETE")

                                <a href="{{ route('post.edit', $post) }}" class="btn btn-sm btn-primary">Edit</a>

                                <button type="submit" class="btn btn-sm btn-warning">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <div class="card-footer text-right">
            <nav class="d-inline-block">
                {{ $posts->links()}}
            </nav>
        </div>
    </div>
</div>

@endsection