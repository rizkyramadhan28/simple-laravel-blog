@extends('admin.index')

@section('title', $title)

@section('content')

<div class="col-12">
    <div class="card">
        @include('admin.partials.alerts')

        <div class="card-header">
            <h4>{{ $title }}</h4>
        </div>

        <div class="card-body">
            @if (count($posts))
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tr>
                        <th>No.</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Tags</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>

                    @foreach ($posts as $post)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->category->name }}</td>
                        <td>
                            @foreach ($post->tags as $tag)
                            <div class="badge badge-pill badge-primary mb-1">{{ $tag->name }}</div>
                            @endforeach
                        </td>
                        <td>
                            <img src="{{ asset($post->image) }}" alt="{{ $post->title }}" class="img-thumbnail"
                                style="height: 100px;">
                        </td>
                        <td>
                            <form action="{{ route('post.trashed.destroy', $post->id) }}" method="POST">
                                @csrf
                                @method("DELETE")

                                <a href="{{ route('post.trashed.restore', $post->id) }}"
                                    class="btn btn-sm btn-primary">Restore</a>

                                <button type="submit" class="btn btn-sm btn-warning">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            @else
            <div class="buttons">
                <p>There are no trashed posts yet.</p>
            </div>
            @endif
        </div>

        <div class="card-footer text-right">
            <nav class="d-inline-block">
                {{ $posts->links()}}
            </nav>
        </div>
    </div>
</div>

@endsection