@extends('admin.index')

@section('title', $title)

@section('content')
<div class="row">
    <div class="col-12">
        @if ($errors->any())
        <div class="alert alert-danger">
            <p><b>Error</b></p>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="card">
            <div class="card-header">
                <h4>{{ $title }}</h4>
            </div>

            <div class="card-body">
                <form action="{{ route('user.update', $user) }}" method="POST">
                    @csrf
                    @method("PUT")

                    <div class="form-group">
                        <label>Name</label>
                        <input value="{{ $user->name }}" type="text" name="name" class="form-control"
                            placeholder="Please enter user's name">
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input value="{{ $user->email }}" type="email" name="email" class="form-control"
                            placeholder="Please enter user's email">
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control"
                            placeholder="Please enter user's password">
                        <p class="text-danger">Please leave it blank if you want to keep the old password.</p>
                    </div>

                    <div class="form-group">
                        <label>Role</label>
                        <select name="role" class="form-control">
                            <option value="1">Admin</option>
                            <option value="0">Author</option>
                        </select>
                    </div>

                    <button class="btn btn-primary float-right">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection