@extends('admin.index')

@section('title', $title)

@section('content')

<div class="col-12">
    <div class="card">
        @include('admin.partials.alerts')

        <div class="card-header">
            <h4>{{ $title }}</h4>

            <div class="card-header-action">
                <a href="{{ route('user.create') }}" class="btn btn-success">Create</a>
            </div>
        </div>

        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Action</th>
                    </tr>

                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            @if ($user->role)
                            <div class="badge badge-info">Admin</div>
                            @else
                            <span class="badge badge-secondary">Author</span>
                            @endif
                        </td>
                        <td>
                            <form action="{{ route('user.destroy', $user) }}" method="POST">
                                @csrf
                                @method("DELETE")

                                <a href="{{ route('user.edit', $user) }}" class="btn btn-sm btn-primary">Edit</a>

                                <button type="submit" class="btn btn-sm btn-warning">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>

        <div class="card-footer text-right">
            <nav class="d-inline-block">
                {{ $users->links()}}
            </nav>
        </div>
    </div>
</div>

@endsection