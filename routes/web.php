<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', "ClientController@index");

Route::group(["middleware" => "auth"], function() {
    // category
    Route::resource("/category", "CategoryController", ["except" => ["show"]]);
    Route::resource("/tag", "TagController", ["except" => ["show"]]);

    // post
    Route::get("/post/trashed", "PostController@showTrashedPosts")->name("post.trashed");
    Route::get("/post/trashed/{post}", "PostController@restoreTrashedPosts")->name("post.trashed.restore");
    Route::delete("/post/trashed/{post}", "PostController@deleteTrashedPost")->name("post.trashed.destroy");
    Route::resource("/post", "PostController");

    // user
    Route::resource("/user", "UserController");

    // home
    Route::get("/home", "HomeController@index")->name("home");
});

Route::fallback(function () {
    return view("pages.not-found");
});